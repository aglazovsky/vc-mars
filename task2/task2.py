#!/bin/python

import sys

def sel(data):
  max_weight = 1000
  d = []
  bag = []
  keys = ['id', 'w', 't', 'c']
  for line in data:
    d.append( {k:int(v) for k,v in zip(keys, line.split(';')) })

  d = sorted(d, key= lambda k:k['c'], reverse=True)
  
  for i in d:
    if (max_weight - i['w']) >= 0:  
      bag.append( str(i['id']))
      max_weight -= i['w']

  return ';'.join(bag)

def main():
  equip = sys.stdin.read().splitlines()

  print( sel(equip))


if __name__ == '__main__':
  main()
