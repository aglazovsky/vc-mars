#!/usr/bin/python

import base64
import json

def main():

  file = open('task1.txt')
  data = base64.decodestring(file.read())
  data = json.loads(data)
  decoded = ''.join([ el['s'] for el in sorted(data) ])
  file.close()

  print(decoded)

if __name__ == '__main__':
  main()
